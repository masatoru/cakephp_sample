<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * Images Controller
 *
 * @property \App\Model\Table\ImagesTable $Images
 */
class ImagesController extends AppController
{

    function __savefile_from_basa64( $base64, $outpath ){

        $res['result'] ='ok';

        $file = new File($outpath,true, 0777);        //WWW_ROOT.'path/test_write.txt'
        $file = $file->open('w', true);

        if(!$file->writable()) {
            $res['msg'] = 'ファイルに書き込み権限がありません。';
            return $res;
        }
        $file->write(base64_decode($base64));   // POST側はbase64_encode( file_get_contents( $imgpath ) );
        $file->close();

        return $res;
    }

    public function testwrite()
    {
        $path = './download/1.txt';
        $file = new File($path,true);        //WWW_ROOT.'path/test_write.txt'
        $file->write('aaaaaaaaa');   // POST側はbase64_encode( file_get_contents( $imgpath ) );
        $file->close();

        $res = array(
            'result' => 'ok'
            );
        $this->set(compact('res'));
    }

    /**
    * 画像を追加する
    */
    public function addimage()
    {
        $res = array('result' => 'error');
        if ($this->request->is('post')) {

            //ファイルに保存する
            $base64 = $this->request->data['base64'];

            $imgdir = '.' . DS . 'download';
            if( !file_exists($imgdir) ) mkdir($imgdir);     //権限で動作しない場合あり
            $outpath = $imgdir . DS . 'fname.png';    

            $file = new File($outpath,true, 0777);        //WWW_ROOT.'path/test_write.txt'
            if(!$file->writable()) {
                $res['msg'] = 'ファイルに書き込み権限がありません。';
                return $res;
            }
            $file->write(base64_decode($base64));   // POST側はbase64_encode( file_get_contents( $imgpath ) );
            $file->close();

        }

        $this->set(compact('res'));
    }
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $images = $this->paginate($this->Images);

        $this->set(compact('images'));
        //$this->set('_serialize', ['users']);  //これいらない

        $this->Crud->on('beforePaginate', function($event) {

            // Search Pluginの検索条件の追加
            $event->subject->query = $this->Images->find()
                ->where($this->request->query);

            //debug($this->request->query,false);    

        });
        return $this->Crud->execute();        

    }


    /**
     * View method
     *
     * @param string|null $id Image id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => []
        ]);
        $this->set('image', $image);
        $this->set('_serialize', ['image']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $image = $this->Images->newEntity();
        if ($this->request->is('post')) {
            $image = $this->Images->patchEntity($image, $this->request->data);
            if ($this->Images->save($image)) {
                $this->Flash->success(__('The image has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The image could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('image'));
        $this->set('_serialize', ['image']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Image id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $image = $this->Images->patchEntity($image, $this->request->data);
            if ($this->Images->save($image)) {
                $this->Flash->success(__('The image has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The image could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('image'));
        $this->set('_serialize', ['image']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Image id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $image = $this->Images->get($id);
        if ($this->Images->delete($image)) {
            $this->Flash->success(__('The image has been deleted.'));
        } else {
            $this->Flash->error(__('The image could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
