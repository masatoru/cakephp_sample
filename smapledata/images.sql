DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`fname` VARCHAR(255) DEFAULT '' NOT NULL     comment '画像ファイル名',
INDEX idx_onkun (`fname`),
PRIMARY KEY (`id`)
)COMMENT='音訓DB' ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `images` (`id`, `fname`) VALUES
(1,'test.png');
